module ApplicationHelper
  def javascript_initializer
    controller = full_controller_name.gsub(' ', '.')
    action = action_name

    root = "window.project"
    init_root = "#{root}.init();\n"

    init_controller = "if(#{root}.#{controller}) { \n"
    init_controller += "\tif(#{root}.#{controller}.init) { #{root}.#{controller}.init(); }\n"
    init_controller += "\tif(#{root}.#{controller}.init_#{action_name}) { #{root}.#{controller}.init_#{action_name}(); }\n"
    init_controller += "}"

    init_root + init_controller
  end
end
