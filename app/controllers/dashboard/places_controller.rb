class Dashboard::PlacesController < ApplicationController
  before_action :authenticate_user!

  def index
    @places = current_user.places.all
  end

  def show
    @place = current_user.places.find(params[:id])
  end

  def new
    @place = Place.new
  end

  def create
    @place = current_user.places.new(place_params)
    if @place.save
      redirect_to dashboard_places_path
    else
      render :new
    end
  end

  def edit
    @place = current_user.places.find(params[:id])
  end

  def update
    @place = current_user.places.find(params[:id])

    if @place.update(place_params)
      redirect_to dashboard_places_path
    else
      :edit
    end
  end

  def destroy
    @place = current_user.places.find(params[:id])
    @place.destroy

    redirect_to dashboard_places_path
  end

  private
  def place_params
    params.require(:place).permit(:name, :full_address, :latitude, :longitude)
  end

end
