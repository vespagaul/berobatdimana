class Api::NearbyController < ApplicationController
  layout false

  def index
    @nearbys = Place.near([params[:lat], params[:lon]],100) 
  end
end
