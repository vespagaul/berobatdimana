var latitude, longitude, address;
var map, infoWindow, point;
var mapOptions = {
  zoom: 18
};

window.project.home = {
  init: function(){
    var ready;
    ready = function() {
      function initialize() {
        if(navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            latitude    = position.coords.latitude;
            longitude   = position.coords.longitude;
            window.project.home.getAddress(latitude, longitude);
            window.project.helper.getNearby(latitude, longitude);
          }, function() {
            window.project.home.handleNoGeolocation(true);
          });
        } else {
          window.project.home.handleNoGeolocation(false);
        }
      }

      if (window.project.helper.isCookiesExist()) {
        latitude  = $.cookie("latitude");
        longitude = $.cookie("longitude");
        address   = $.cookie("address");
        
        // window.project.home.setMap(latitude, longitude, address);
        window.project.home.setAttributes(latitude, longitude, address);
      } else {
        initialize();
      }
      
      window.project.helper.getNearby(latitude, longitude);
      
      $("#change-address").click(function(){
        $('#change-address-modal').foundation('reveal', 'open');
        window.project.home.setMap(latitude, longitude, address);
        google.maps.event.trigger(map, 'resize');
        
        $('#change-address-modal').on('opened.fndtn.reveal', '[data-reveal]', function(){ 
          google.maps.event.trigger(map, 'resize');
          alert('ok');
        });
      });
    };
$(document).on('open.fndtn.reveal', '#change-address-modal', function () {
  console.log($(this));
  alert('crot');
});
    //
    $(document).ready(ready);
    // $(document).on('page:load', ready);
    // $("#map-canvas").height($(window).height() - 60);
  },
  
  getAddress: function(latitude, longitude) {
    var geocoder  = new google.maps.Geocoder();
    var point     = new google.maps.LatLng(latitude, longitude);
    var address   = '';
    geocoder.geocode({latLng: point}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          address = results[0].formatted_address;              
          // window.project.home.setMap(latitude, longitude, address);
          window.project.home.setAttributes(latitude, longitude, address);
          window.project.helper.setCookies(latitude, longitude, address);
        }
      }
    });

    return address;
  },

  setMap: function(latitude, longitude, address) {
    map = new google.maps.Map(document.getElementById('map-canvas'),
                              mapOptions);
    point = new google.maps.LatLng(latitude, longitude);

    // infoWindow  = new google.maps.InfoWindow();    
    // infoWindow.setContent(address);
    // infoWindow.setPosition(point);
    // infoWindow.open(map);
    //
    marker = new google.maps.Marker({
      position: point,
      map: map,
      draggable: true,
      title: address
    });

    map.setCenter(point);
  },

  setAttributes: function(latitude, longitude, address){
    $("#latitude").html(latitude);
    $("#longitude").html(longitude);       
    $("#address").html(address);
  },

  handleNoGeolocation: function(errorFlag) {
    if (errorFlag) {
      var content = 'Error: The Geolocation service failed.';
    } else {
      var content = 'Error: Your browser doesn\'t support geolocation.';
    }

    var mapOptions = {
      zoom: 18
    };
    
    map = new google.maps.Map(document.getElementById('map-canvas'),
                              mapOptions);

    var options = {
      map: map,
      position: new google.maps.LatLng(-6.3099152, 106.89036580000001),
      content: content
    };

    var infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
  }

};
