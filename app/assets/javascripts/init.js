var latitude, longitude, address;
var map, infoWindow, point, marker, geocoder;
var mapOptions = {
  zoom: 18
};

window.project = {
  init: function() {
  },

  home: {},
  relocations: {},
  dashboard: {}
};

window.project.helper = {
  isCookiesExist: function(){
    if ($.cookie("latitude") == undefined || $.cookie("longitude") == undefined || $.cookie("address") == undefined) {
      return false;
    } else {
      return true;
    }
  },

  setCookies: function(latitude, longitude, address){
    $.cookie("latitude", latitude);
    $.cookie("longitude", longitude);
    $.cookie("address", address);
  },

  getNearby: function(latitude, longitude) {
    $.get("/api/nearby?lat=" + latitude + "&lon=" + longitude,
          function(data) {
      $("#nearby-container").html(data);
    });
  }

};
