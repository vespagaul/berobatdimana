window.project.relocations = {
  init: function(){
    var ready;
    ready = function() {
      function initialize() {
        if(navigator.geolocation) {
          navigator.geolocation.getCurrentPosition(function(position) {
            latitude    = position.coords.latitude;
            longitude   = position.coords.longitude;
            window.project.home.getAddress(latitude, longitude);
          }, function() {
            window.project.home.handleNoGeolocation(true);
          });
        } else {
          window.project.home.handleNoGeolocation(false);
        }
      }

      if (window.project.helper.isCookiesExist()) {
        latitude  = $.cookie("latitude");
        longitude = $.cookie("longitude");
        address   = $.cookie("address");
        
        window.project.home.setMap(latitude, longitude, address);
        window.project.home.setAttributes(latitude, longitude, address);
      } else {
        initialize();
      }      
    };

    $("#map-button-find").on("click", function(){
      
      window.project.relocations.findAddress($("#map-address").val());
    });

   //
    $(document).ready(ready);
    $(document).on('page:load', ready);
    // $("#map-canvas").height($(window).height() - 60);
  },
  
  getAddress: function(latitude, longitude) {
    var geocoder  = new google.maps.Geocoder();
    var point     = new google.maps.LatLng(latitude, longitude);
    var address   = '';
    geocoder.geocode({latLng: point}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
          address = results[0].formatted_address;              
          window.project.home.setMap(latitude, longitude, address);
          window.project.home.setAttributes(latitude, longitude, address);
          window.project.helper.setCookies(latitude, longitude, address);
        }
      }
    });

    return address;
  },

  findAddress: function(address) {
    alert(address);
    if (address == '') return;

    geocoder = new google.maps.Geocoder();
    geocoder.geocode({'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        console.log(results);

          //map.setCenter(results[0].geometry.location);
          latitude = results[0].geometry.location.lat().toFixed(6);
          longitude = results[0].geometry.location.lng().toFixed(6);
          // var latlong = "(" + results[0].geometry.location.lat().toFixed(6) + ", " +
          //         +results[0].geometry.location.lng().toFixed(6)+ ")";
          //
          // document.getElementById('latlngspan').innerHTML =  latlong;
          // document.getElementById('coordinatesurl').value = 'http://www.latlong.net/c/?lat='
                  // + results[0].geometry.location.lat().toFixed(6) + '&long='
                  // + results[0].geometry.location.lng().toFixed(6);
                  //
          // marker.setPosition(results[0].geometry.location);
          // map.setZoom(16);
          // infoWindow.setContent(latlong);
          //
          // if (infoWindow) {
          //     infoWindow.close();
          // }
          //
          // google.maps.event.addListener(marker, 'click', function() {
          //     infoWindow.open(map, marker);
          // });
          //
          // infoWindow.open(map, marker);

          //dec2dms();
          
          window.project.relocations.getAddress(latitude, longitude);

      } else {
          alert("Lat and long cannot be found.");
      }
    });

    //var geocoder  = new google.maps.Geocoder();
    // geocoder.getLatLng(
    //   address,
    //   function(point) {
    //     if (!point) {
    //       alert('Address not found');
    //     } else {
    //       latitude = point.lat().toFixed(5);
    //       longitude = point.lng().toFixed(5);
    //       window.project.relocations.getAddress(latitude, longitude);
    //     }
    //   }
    // );
  },

  setMap: function(latitude, longitude, address) {

    
    map = new google.maps.Map(document.getElementById('map-canvas'),
                              mapOptions);
    point = new google.maps.LatLng(latitude, longitude);

    infoWindow  = new google.maps.InfoWindow();    
    infoWindow.setContent(address);
    infoWindow.setPosition(point);
    infoWindow.open(map);

    map.setCenter(point);
    

  },

  setAttributes: function(latitude, longitude, address){
    $("#latitude").html(latitude);
    $("#longitude").html(longitude);       
    $("#address").html(address);
  },

  handleNoGeolocation: function(errorFlag) {
    if (errorFlag) {
      var content = 'Error: The Geolocation service failed.';
    } else {
      var content = 'Error: Your browser doesn\'t support geolocation.';
    }

    var mapOptions = {
      zoom: 18
    };
    
    map = new google.maps.Map(document.getElementById('map-canvas'),
                              mapOptions);

    var options = {
      map: map,
      position: new google.maps.LatLng(-6.3099152, 106.89036580000001),
      content: content
    };

    var infowindow = new google.maps.InfoWindow(options);
    map.setCenter(options.position);
  }

};
