//= require jquery
//= require jquery_ujs
//= require foundation
//= require turbolinks
//= require jquery.cookie

//= require init
//= require home
//= require relocations
//= require_tree ./dashboard

$(function(){ $(document).foundation(); });
