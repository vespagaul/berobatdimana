class Place < ActiveRecord::Base
  validates :name, :full_address, :latitude, :longitude, presence: true
  reverse_geocoded_by :latitude, :longitude, :address => :geocode_address
  after_validation :reverse_geocode

  belongs_to :user
end
