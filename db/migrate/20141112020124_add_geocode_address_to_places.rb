class AddGeocodeAddressToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :geocode_address, :string
  end
end
